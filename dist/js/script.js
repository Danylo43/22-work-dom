"use stric"

//1. Знайдіть всі елементи з класом "feature", запишіть 
//в змінну, вивести в консоль. Використайте 2 способи 
//для пошуку елементів. Задайте кожному елементу з 
//класом "feature" вирівнювання тексту по - 
//центру(text-align: center).

//1 Спосіб
const getClass = document.querySelectorAll(".feature");
getClass.forEach((element) => {
  element.style.textAlign = "center";
});

//2 Спосіб
const getElement = document.getElementsByClassName("feature");
for (let i = 0; i < getElement.length; i++) {
  getElement[i].style.textAlign = "center";
}

//2. Змініть текст усіх елементів h2 на "Awesome feature".
const getText = document.getElementsByTagName("h2");
for (let i = 0; i < getText.length; i++) {
  getText[i].textContent = "Awesome feature";
}

//3. Знайдіть всі елементи з класом "feature-title" та 
//додайте в кінець тексту елементу знак оклику "!".
const newText = document.querySelectorAll(".feature-title");
newText.forEach(element => {
  element.textContent += " !";
});

