import gulp from "gulp";
const { src, dest, series, parallel, watch } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import * as sass from "sass";
import gulpSass from "gulp-sass";
import bsc from "browser-sync";
const browserSync = bsc.create();
const sassCompiler = gulpSass(sass);

const jsTaskHandler = () => {
  return src("./src/js/**/*.js")
    .pipe(dest("./dist/js"))
    .pipe(browserSync.stream());
};

const htmlTaskHandler = () => {
  return src("./src/html/*.html")
    .pipe(dest("./dist"))
    .pipe(browserSync.stream());
};

const cssTaskHandler = () => {
  return src("./src/scss/**/*.scss")
    .pipe(sassCompiler().on("error", sassCompiler.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest("./dist/css"))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src("./src/images/**/*.*")
    .pipe(imagemin())
    .pipe(dest("./dist/images"));
};

const cleanDistTaskHandler = () => {
  return src("./dist", { allowEmpty: true, read: false }).pipe(clean());
};

const faviconTaskHandler = () => {
  return src("./src/favicon/**/*").pipe(dest("./dist/favicon"));
};

function watcher() {
  watch("./src/html/*.html", series(htmlTaskHandler));
  watch("./src/scss/**/*.{scss,sass,css}", series(cssTaskHandler));
  watch("./src/js/**/*.js", series(jsTaskHandler));
  watch("./src/images/**/*.*", series(imagesTaskHandler));
}

function browserSyncTaskHandler() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
  watch("./dist/**/*").on("change", browserSync.reload);
}

export const favicon = faviconTaskHandler;
export const js = jsTaskHandler;
export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const images = imagesTaskHandler;

export const build = series(
  cleanDistTaskHandler,
  parallel(
    htmlTaskHandler,
    cssTaskHandler,
    imagesTaskHandler,
    faviconTaskHandler,
    jsTaskHandler
  )
);

export const dev = series(build, parallel(browserSyncTaskHandler, watcher));
